# **Assignment 3**

Ivan Cattaneo 793053 <br />
Antonio De Carolis 795559 <br />
Luca Palazzi 793556

### **Installazione del progetto**

Il progetto memorizza le entità su un database **PostgreSQL 10.5** e viene utilizzato tramite un tool di interfaccia grafica chiamato **PgAdmin** 4, che viene scaricato insieme al DBMS.
Per eseguire il progetto deve essere creato, alla porta di default di PostgreSQL (5432), un database chiamato 
ecommerce con credenziali d’accesso (username e password) rispettivamente denominate postgres e root. 
Per modificare il nome del database, l’username e la password è necessario cambiare manualmente il file persistence.xml, 
presente in src/main/java/META-INF, modificando l’attributo value dei seguenti tags:

```
<property name="javax.persistence.jdbc.url" value="jdbc:postgresql://localhost:5432/ecommerce" />
<property name="javax.persistence.jdbc.user" value="postgres" />
<property name="javax.persistence.jdbc.password" value="root" />
```
Per installare il progetto, dando per scontato l’utilizzo di **Eclipse** come IDE, devono essere eseguiti i seguenti passi 
(la repository con il codice sorgente è accessibile al seguente [link](https://gitlab.com/i.cattaneo8/assignment3.git)):

* nella sezione Package Explorer cliccare su tasto destro e Import
* selezionare la cartella Git -> Projects from Git
* selezionare Clone URI ed inserire il link precedentemente comunicato
* cliccare su Next ed inserire le credenziali d’accesso a Gitlab
* cliccare nuovamente Next per due volte mantenendo il branch master
* reinserire le credenziali d’accesso a Gitlab -> Next -> Finish


### **Esecuzione del progetto**

Il progetto possiede un’interfaccia grafica. Per utilizzarla, dopo aver eseguito la procedura di installazione, 
è necessario cliccare con il tasto destro sul nome del progetto nella sezione Package Explorer, 
selezionare Run As -> Run on Server. È stato utilizzato **Tomcat v7.0.**
Per eseguire i test JUnit, invece, è necessario andare in src/test/java, 
cliccare sul tasto destro sul package test, selezionare Run As -> JUnit Test.
