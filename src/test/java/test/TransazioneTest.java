package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import entities.Produttore;
import entities.Smartphone;
import entities.Utente;
import entities.Transazione;
import services.ProduttoreService;
import services.SmartphoneService;
import services.TransazioneService;
import services.UtenteService;

public class TransazioneTest {

	private static UtenteService utenteService;
	private static SmartphoneService smartphoneService;
	private static ProduttoreService produttoreService;
	private static TransazioneService transazioneService;

	@BeforeClass
	public static void beforeClass() {
		utenteService = new UtenteService();
		smartphoneService = new SmartphoneService();
		produttoreService = new ProduttoreService();
		transazioneService = new TransazioneService();
	}
	
	@Before
	public void before() {
		utenteService.initEntityManager();
		utenteService.deleteAll();
		smartphoneService.initEntityManager();
		smartphoneService.deleteAll();
		produttoreService.initEntityManager();
		produttoreService.deleteAll();
		transazioneService.initEntityManager();
		transazioneService.deleteAll();
	}
	
	@Test
	public void create() {
		Transazione t = createTransaction();
		Transazione c = transazioneService.read(t.getId());
		assertNotNull(c.getId());
		assertEquals(c.getUtente(), t.getUtente());
		transazioneService.deleteAll();
		smartphoneService.deleteAll();
		utenteService.deleteAll();
		produttoreService.deleteAll();

	}
	
	@Test
	public void read() {
		Transazione t = createTransaction();
		Transazione c = transazioneService.read(t.getId());
		assertEquals(c.getId(), t.getId());
		transazioneService.deleteAll();
		smartphoneService.deleteAll();
		utenteService.deleteAll();
		produttoreService.deleteAll();
	}
	
	@Test
	public void readAll() {
		Utente u = new Utente("Ivan", "Cattaneo", "Email@email.it", "Carta di credito", null);
		utenteService.create(u);
		Produttore p = new Produttore("Samsung", 12, null, null);
	    produttoreService.create(p);
	    Smartphone s1 = new Smartphone(p, "Galaxy s10", 2017, 659.95f, null);
		smartphoneService.create(s1);
		Smartphone s2 = new Smartphone(p, "Galaxy s11", 2018, 1069.95f, null);
		smartphoneService.create(s2);
		Transazione t1 = new Transazione(u, s1, 2);
		transazioneService.create(t1);
		Transazione t2 = new Transazione(u, s2, 4);
		transazioneService.create(t2);
		List<Transazione> transazioni = transazioneService.readAll();
		assertEquals(2, transazioni.size());
		transazioneService.deleteAll();
		smartphoneService.deleteAll();
		utenteService.deleteAll();
		produttoreService.deleteAll();
	}
	
	@Test
	public void update() {
		Transazione t = createTransaction();
		Transazione c = transazioneService.read(t.getId());
		c.setQuantita(3);
		transazioneService.update(c);
		int update = transazioneService.read(t.getId()).getQuantita();
		assertEquals(update, 3);
		transazioneService.deleteAll();
		smartphoneService.deleteAll();
		utenteService.deleteAll();
		produttoreService.deleteAll();
	}
		
	@Test
	public void delete() {
		Transazione t = createTransaction();
		transazioneService.delete(t);
		assertEquals(transazioneService.readAll().size(), 0);
		transazioneService.deleteAll();
		smartphoneService.deleteAll();
		utenteService.deleteAll();
		produttoreService.deleteAll();
	}
	
	@After
	public void after() {
		utenteService.deleteAll();
		utenteService.closeEntityManager();
	}
	
	public Transazione createTransaction() {
		Utente u = new Utente("Ivan", "Cattaneo", "Email@email.it", "Carta di credito", null);
		utenteService.create(u);
		Produttore p = new Produttore("Samsung", 12, null, null);
	    produttoreService.create(p);
	    Smartphone s = new Smartphone(p, "Galaxy s10", 2017, 659.95f, null);
		smartphoneService.create(s);
		Transazione t = new Transazione(u, s, 2);
		transazioneService.create(t);
		return t;
	}
		
}
