package test;

import services.ProduttoreService;
import services.SmartphoneService;
import entities.Produttore;
import entities.Smartphone;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;
import java.util.List;

public class SmartphoneTest {
	
	private static SmartphoneService smartphoneService;
	private static ProduttoreService produttoreService;

	
	@BeforeClass
	public static void beforeClass() {
		smartphoneService = new SmartphoneService();
		produttoreService = new ProduttoreService();
	}
	
	@Before
	public void before() {
		smartphoneService.initEntityManager();
		smartphoneService.deleteAll();
		produttoreService.initEntityManager();
		produttoreService.deleteAll();
	}
	
	@Test
	public void create() throws Exception {
	    Produttore p = new Produttore("Samsung", 12, null, null);
	    produttoreService.create(p);
	    Smartphone s = new Smartphone(p, "Galaxy s10", 2017, 659.95f, null);
		smartphoneService.create(s);
		Smartphone c = smartphoneService.read(s.getId());
		assertNotNull(c.getId());
		assertEquals(c.getPrezzo(), s.getPrezzo(), 0.001);
		smartphoneService.deleteAll();
		produttoreService.deleteAll();
	}
	
	@Test
	public void read() {
		Produttore p = new Produttore("Samsung", 12, null, null);
	    produttoreService.create(p);
	    Smartphone s = new Smartphone(p, "Galaxy s10", 2017, 659.95f, null);
		smartphoneService.create(s);
		Smartphone c = smartphoneService.read(s.getId());
		assertEquals(c.getId(), s.getId());
		smartphoneService.deleteAll();
		produttoreService.deleteAll();
	}
	
	@Test
	public void readAll() {
		Produttore p = new Produttore("Samsung", 12, null, null);
	    produttoreService.create(p);
	    Smartphone s1 = new Smartphone(p, "Galaxy s10", 2017, 659.95f, null);
		smartphoneService.create(s1);
		Smartphone s2 = new Smartphone(p, "Galaxy s11", 2018, 1069.95f, null);
		smartphoneService.create(s2);
		List<Smartphone> smartphones = smartphoneService.readAll();
		assertEquals(2, smartphones.size());
		smartphoneService.deleteAll();
		produttoreService.deleteAll();
	}
	
	@Test
	public void update() throws Exception {
		Produttore p = new Produttore("Samsung", 12, null, null);
	    produttoreService.create(p);
	    Smartphone s = new Smartphone(p, "Galaxy s10", 2017, 659.95f, null);
		smartphoneService.create(s);
		Smartphone c = smartphoneService.read(s.getId());
		c.setAnno(2018);
		smartphoneService.update(c);
		int update = smartphoneService.read(s.getId()).getAnno();
		assertEquals(update, 2018);
		smartphoneService.deleteAll();
		produttoreService.deleteAll();
	}
		
	@Test
	public void delete() {
		Produttore p = new Produttore("Samsung", 12, null, null);
	    produttoreService.create(p);
	    Smartphone s = new Smartphone(p, "Galaxy s10", 2017, 659.95f, null);
		smartphoneService.create(s);
		smartphoneService.delete(s);
		assertEquals(smartphoneService.readAll().size(), 0);
		produttoreService.deleteAll();
	}
	
	@Test
	public void search() {
		Produttore p = new Produttore("Samsung", 12, null, null);
	    produttoreService.create(p);
	    Smartphone s1 = new Smartphone(p, "Galaxy s10", 2017, 659.95f, null);
		smartphoneService.create(s1);
		Smartphone s2 = new Smartphone(p, "Galaxy s11", 2018, 1069.95f, null);
		smartphoneService.create(s2);
		List<Smartphone> search = smartphoneService.search("modello", "Galaxy s10");
		assertEquals(search.size(), 1);
		smartphoneService.deleteAll();
		produttoreService.deleteAll();
	}
	
	@After
	public void afterClass() {
		smartphoneService.deleteAll();
		smartphoneService.closeEntityManager();
	}
	
}