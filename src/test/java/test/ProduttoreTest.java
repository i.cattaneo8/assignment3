package test;

import services.ProduttoreService;
import entities.Produttore;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;
import java.util.List;

public class ProduttoreTest {
	
	private static ProduttoreService produttoreService = new ProduttoreService();

	@BeforeClass
	public static void beforeClass() {
		produttoreService = new ProduttoreService();
	}
	
	@Before
	public void before() {
		produttoreService.initEntityManager();
		produttoreService.deleteAll();
	}
	
	@Test
	public void create() throws Exception {
		Produttore p = new Produttore("Samsung", 12, null, null);
	    produttoreService.create(p);
	    Produttore c = produttoreService.read(p.getId());
		assertNotNull(c.getId());
		assertEquals(c.getNumeroModelli(), p.getNumeroModelli());
		produttoreService.deleteAll();
	}
	
	@Test
	public void read() {
		Produttore p = new Produttore("Samsung", 12, null, null);
	    produttoreService.create(p);
	    Produttore c = produttoreService.read(p.getId());
	    assertEquals(c.getId(), p.getId());
	    produttoreService.deleteAll();
	}
	
	@Test
	public void readAll() {
		Produttore p1 = new Produttore("Samsung", 12, null, null);
		Produttore p2 = new Produttore("Apple", 7, null, null);
		produttoreService.create(p1);
		produttoreService.create(p2);
		List<Produttore> produttori = produttoreService.readAll();
		assertEquals(2, produttori.size());
		produttoreService.deleteAll();
	}
	
	@Test
	public void update() {
		Produttore p = new Produttore("Samsung", 12, null, null);
	    produttoreService.create(p);
	    Produttore c = produttoreService.read(p.getId());
		c.setNumeroModelli(13);
		produttoreService.update(c);
		int update = produttoreService.read(p.getId()).getNumeroModelli();
		assertEquals(update, 13);
		produttoreService.deleteAll();
	}
		
	@Test
	public void delete() {
		Produttore p1 = new Produttore("Samsung", 12, null, null);
		Produttore p2 = new Produttore("Xiaomi", 12, p1, null);
		Produttore p3 = new Produttore("Redmi", 12, p2, null);
	    produttoreService.create(p1);
	    produttoreService.create(p2);
	    produttoreService.create(p3);
	    produttoreService.delete(p1);
		assertEquals(produttoreService.readAll().size(), 0);
	}
	
	@Test
	public void search() {
		Produttore p1 = new Produttore("Samsung", 12, null, null);
		Produttore p2 = new Produttore("Apple", 7, null, null);
		produttoreService.create(p1);
		produttoreService.create(p2);
		List<Produttore> searched = produttoreService.search("nome", "Samsung".toUpperCase());
		assertEquals(searched.size(), 1);
		produttoreService.deleteAll();
	}
	
	@After
	public void afterClass() {
		produttoreService.deleteAll();
		produttoreService.closeEntityManager();
	}
	
}
