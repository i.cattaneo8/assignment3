package test;

import services.UtenteService;
import entities.Utente;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;
import java.util.List;

public class UtenteTest {
	
	private static UtenteService utenteService;

	@BeforeClass
	public static void beforeClass() {
		utenteService = new UtenteService();
	}
	
	@Before
	public void before() {
		utenteService.initEntityManager();
		utenteService.deleteAll();
	}
	
	@Test
	public void create() {
		Utente u = new Utente("Ivan","Cattaneo","Email@email.it","Carta di credito", null);
		utenteService.create(u);
		Utente c = utenteService.read(u.getId());
		assertNotNull(c.getId());
		assertEquals(c.getEmail(), u.getEmail());
		utenteService.deleteAll();
	}
	
	@Test
	public void read() {
		Utente u = new Utente("Ivan","Cattaneo","Email@email.it","Carta di credito", null);
		utenteService.create(u);
		Utente c = utenteService.read(u.getId());
		assertEquals(c.getId(), u.getId());
		utenteService.deleteAll();
	}
	
	@Test
	public void readAll() {
		Utente u1 = new Utente("Ivan","Cattaneo","Email@email.it","Carta di credito", null);
		Utente u2 = new Utente("Luca","Palazzi","lucapalazzi18@hotmail.it","PayPal", null);
		utenteService.create(u1);
		utenteService.create(u2);
		List<Utente> utenti = utenteService.readAll();
		assertEquals(2, utenti.size());
		utenteService.deleteAll();
	}
	
	@Test
	public void update() {
		Utente u = new Utente("Ivan","Cattaneo","Email@email.it","Carta di credito", null);
		utenteService.create(u);
		Utente c = utenteService.read(u.getId());
		c.setCognome("Palazzi");
		utenteService.update(c);
		String update = utenteService.read(u.getId()).getCognome();
		assertEquals(update, "Palazzi");
		utenteService.deleteAll();
	}
		
	@Test
	public void delete() {
		Utente u = new Utente("Ivan","Cattaneo","Email@email.it","Carta di credito", null);
		utenteService.create(u);
		utenteService.delete(u);
		assertEquals(utenteService.readAll().size(), 0);
	}
	
	@Test
	public void search() {
		Utente u1 = new Utente("Ivan","Cattaneo","Email@email.it","Carta di credito", null);
		Utente u2 = new Utente("Gio","Cattaneo","Email@email.it","Carta di credito", null);
		Utente u3 = new Utente("Ivan","Palazzi","Email@email.it","Carta di credito", null);
		utenteService.create(u1);
		utenteService.create(u2);
		utenteService.create(u3);
		List<Utente> searched = utenteService.search("cognome", "Cattaneo");
		assertEquals(searched.size(), 2);
		utenteService.deleteAll();
	}
	
	@After
	public void after() {
		utenteService.deleteAll();
		utenteService.closeEntityManager();
	}
	
}
