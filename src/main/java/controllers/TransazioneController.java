package controllers;


import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

import entities.Utente;
import entities.Transazione;
import entities.Smartphone;
import services.UtenteService;
import services.TransazioneService;
import services.SmartphoneService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class TransazioneController extends ActionSupport  implements Preparable{
	
	private static final long serialVersionUID = 1L;

	private TransazioneService transazioneService = new TransazioneService();
    private UtenteService utenteService = new UtenteService();
    private SmartphoneService smartphoneService = new SmartphoneService();
    private Transazione transazione;
    private Utente utente;
    private Smartphone smartphone;
    private List<Transazione> transazioni;
    private List<Utente> utenti;
    private List<Smartphone> smartphones;
    private List<String> nomiUtenti = new ArrayList<String>();
    private List<String> modelliSmartphone = new ArrayList<String>();
    private List<Integer> quantita = new ArrayList<Integer>();
    private String utentenome;
    private String smartphonemodello;

    @Override
    public void prepare() throws Exception { 
    	utenti = utenteService.readAll();
    	for(int i = 0; i < utenti.size(); i++) {
    		nomiUtenti.add(utenti.get(i).getEmail());
    	}
    	
    	smartphones = smartphoneService.readAll();
    	for(int i = 0; i < smartphones.size(); i++) {
    		modelliSmartphone.add(smartphones.get(i).getModello());
    	}
    	
    	quantita.addAll(Arrays.asList(1,2,3,4,5,6,7,8,9,10)); 
        if (transazione != null && transazione.getId() != 0) {
        	transazione = transazioneService.read(transazione.getId());
        	utentenome = transazione.getUtente().getEmail();
        	//smartphonemodello = transazione.getSmartphone().getModello();

        	if (transazione.getSmartphone() != null) {
        	smartphonemodello = transazione.getSmartphone().getModello();
        	}
        }
    }
    
    public void validate() {
    	if (utentenome == null) {
    		addFieldError("utentenome", "Non e' possibile aggiungere una transazione se non esistono utenti.");
    	}
    	
    	if (smartphonemodello == null) {
    		addFieldError("smartphonemodello", "Non e' possibile aggiungere una transazione se non esistono modelli.");
    	}
    }
    
	public String list() {
        transazioni = transazioneService.readAll();
        return SUCCESS;
    }
	
	public String save() {
    	for (int i = 0; i < utenti.size(); i++) {
    		if (utenti.get(i).getEmail().equals(utentenome)) {
    			transazione.setUtente(utenti.get(i));    			
    		}
       	}
    	
    	for (int i = 0; i < smartphones.size(); i++) {
    		if(smartphones.get(i).getModello().equals(smartphonemodello)) {
    			transazione.setSmartphone(smartphones.get(i));    			
    		}
    	}
    	
		if (transazioneService.read(transazione.getId()) == null) {
			transazioneService.create(transazione);	
			utente = utenteService.read(transazione.getUtente().getId());
			List<Transazione> t = utente.getTransazione();
			t.add(transazione);
			utente.setTransazione(t);
			utenteService.update(utente);
			smartphone = smartphoneService.read(transazione.getSmartphone().getId());
			List<Transazione> s = smartphone.getTransazione();
			s.add(transazione);
			smartphone.setTransazione(s);
			smartphoneService.update(smartphone);
		} else {	
			transazioneService.update(transazione);
		}

        return SUCCESS;
    }
	
	public String delete() {
		transazioneService.delete(transazione);
        return SUCCESS;
	}

    public List<Transazione> getUtenti() {
		return transazioni;
	}
    
	public Transazione getTransazione() {
		return transazione;
	}
	
	public List<Transazione> getTransazioni() {
		return transazioni;
	}

	public void setTransazioni(List<Transazione> transazioni) {
		this.transazioni = transazioni;
	}

	public void setTransazione(Transazione transazione) {
		this.transazione = transazione;
	}

	public TransazioneService getTransazioneService() {
		return transazioneService;
	}

	public void setTransazioneService(TransazioneService transazioneService) {
		this.transazioneService = transazioneService;
	}

	public List<Smartphone> getSmartphones() {
		return smartphones;
	}

	public void setSmartphones(List<Smartphone> smartphones) {
		this.smartphones = smartphones;
	}

	public List<String> getNomiUtenti() {
		return nomiUtenti;
	}

	public void setNomiUtenti(List<String> nomiUtenti) {
		this.nomiUtenti = nomiUtenti;
	}

	public List<String> getModelliSmartphone() {
		return modelliSmartphone;
	}

	public void setModelliSmartphone(List<String> modelliSmartphone) {
		this.modelliSmartphone = modelliSmartphone;
	}

	public List<Integer> getQuantita() {
		return quantita;
	}

	public void setQuantita(List<Integer> quantita) {
		this.quantita = quantita;
	}

	public String getUtentenome() {
		return utentenome;
	}

	public void setUtentenome(String utentenome) {
		this.utentenome = utentenome;
	}

	public String getSmartphonemodello() {
		return smartphonemodello;
	}

	public void setSmartphonemodello(String smartphonemodello) {
		this.smartphonemodello = smartphonemodello;
	}

	public void setUtenti(List<Utente> utenti) {
		this.utenti = utenti;
	}
	
    public UtenteService getUtenteService() {
		return utenteService;
	}

	public void setUtenteService(UtenteService utenteService) {
		this.utenteService = utenteService;
	}

	public SmartphoneService getSmartphoneService() {
		return smartphoneService;
	}

	public void setSmartphoneService(SmartphoneService smartphoneService) {
		this.smartphoneService = smartphoneService;
	}

	public Utente getUtente() {
		return utente;
	}

	public void setUtente(Utente utente) {
		this.utente = utente;
	}

	public Smartphone getSmartphone() {
		return smartphone;
	}

	public void setSmartphone(Smartphone smartphone) {
		this.smartphone = smartphone;
	}

}