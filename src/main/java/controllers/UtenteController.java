package controllers;


import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

import entities.Utente;
import services.UtenteService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.validator.routines.EmailValidator;


public class UtenteController extends ActionSupport  implements Preparable{
	
	private static final long serialVersionUID = 1L;

    private UtenteService utenteService = new UtenteService();
    private Utente utente;
    private List<Utente> utenti;
    private List<String> metods = new ArrayList<String>();

    @Override
    public void prepare() throws Exception {    
    	metods.addAll(Arrays.asList("PayPal", "Carta di credito","Prepagata", "Bonifico Bancario", "Contrassegno"));
        if (utente != null && utente.getId() != 0) {
        	utente = utenteService.read(utente.getId());
        }
    }
    
    public void validate() {
    	if (!EmailValidator.getInstance().isValid(utente.getEmail())) {
    		addFieldError("utente.email", "Email non valida.");
        }
    	
    	if (!utenteService.readMail(utente.getEmail()) && !(utente.getId() != 0)){
    		addFieldError("utente.email", "Email gia' presente nel db.");
    	}
    	
    	if(utente.getEmail()=="") {
    		addFieldError("utente.email", "L'email non puo' essere vuota.");
    	}
    	
    	if(utente.getNome()=="") {
    		addFieldError("utente.nome", "Il nome non puo' essere vuoto");
    	}
    	
    	if(utente.getCognome()=="") {
    		addFieldError("utente.cognome", "Il cognome non puo' essere vuoto");
    	}
    	
    }
    
	public String list() {
        utenti = utenteService.readAll();
        return SUCCESS;
    }
	
	public String save() {
		System.out.println(utente.getId());
		if(utenteService.read(utente.getId())==null) {
			utenteService.create(utente);
		} else {
			utenteService.update(utente);
		}
        return SUCCESS;
    }
	
	public String delete() {
		utenteService.delete(utente);
        return SUCCESS;
	}

    public List<Utente> getUtenti() {
		return utenti;
	}
    
	public Utente getUtente() {
		return utente;
	}

	public void setUtente(Utente utente) {
		this.utente = utente;
	}

	public List<String> getMetods() {
		return metods;
	}

	public void setMetods(List<String> metods) {
		this.metods = metods;
	}

}

