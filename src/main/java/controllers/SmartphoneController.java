package controllers;


import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

import entities.Produttore;
import entities.Smartphone;
import services.ProduttoreService;
import services.SmartphoneService;

import java.util.ArrayList;
import java.util.List;


public class SmartphoneController extends ActionSupport  implements Preparable{
	
	private static final long serialVersionUID = 1L;

    private SmartphoneService smartphoneService = new SmartphoneService();
    private ProduttoreService produttoreService = new ProduttoreService();
    private Smartphone smartphone;
    private Produttore produttore;
    private List<Smartphone> smartphones;
    private List<Produttore> produttori;
    private List<String> nomiProduttori = new ArrayList<String>();
    private String product;

    @Override
    public void prepare() throws Exception { 
    	produttori = produttoreService.readAll();
    	for (int i = 0; i < produttori.size(); i++) {
    		nomiProduttori.add(produttori.get(i).getNome());	
    	}
    	
        if (smartphone != null && smartphone.getId() != 0) {
        	smartphone = smartphoneService.read(smartphone.getId());
        	product = smartphone.getProduttore().getNome();
        }
    }
    
    public void validate() {
    	if (product == null) {
    		addFieldError("product", "Non e' possibile aggiungere un modello se non esistono produttori.");
    	}
    	
    	if (smartphone.getModello() == "") {
    		addFieldError("smartphone.modello", "Il modello dello smartphone non puo' essere vuoto.");
    	}
    	
    	if (smartphone.getAnno() == null ||smartphone.getAnno() < 2000) {
    		addFieldError("smartphone.anno", "L'anno non puo' essere vuoto o inferiore all'anno 2000.");
    	}
    	
    	if (smartphone.getAnno() == null || smartphone.getAnno() > 2019) {
    		addFieldError("smartphone.anno", "L'anno non puo' essere vuoto o superiore all'anno 2019.");
    	}
    	
    	if (smartphone.getPrezzo() <= 0) {
    		addFieldError("smartphone.prezzo", "Il prezzo deve essere maggiore di 0.");
    	}
    	
    }
    
	public String list() {
        smartphones = smartphoneService.readAll();
        return SUCCESS;
    }
	
	public String save() {
    	for (int i = 0; i < produttori.size(); i++) {
    		if (produttori.get(i).getNome().equals(product)) {
    			smartphone.setProduttore(produttori.get(i));    			
    		}
    	}
    	
		if (smartphoneService.read(smartphone.getId()) == null) {
			smartphoneService.create(smartphone);
			produttore = produttoreService.read(smartphone.getProduttore().getId());
			List<Smartphone> s = produttore.getSmartphone();
			s.add(smartphone);
	        produttore.setNumeroModelli(produttore.getNumeroModelli()+1);
			produttore.setSmartphone(s);
			produttoreService.update(produttore);
		} else {	
			smartphoneService.update(smartphone);
		}
		
        return SUCCESS;
    }
	
	public String delete() {
		produttore = produttoreService.read(smartphone.getProduttore().getId());
		System.out.println(produttore);
		produttore.setNumeroModelli(produttore.getNumeroModelli()-1);
		produttoreService.update(produttore);
		smartphoneService.delete(smartphone);
        return SUCCESS;
	}

	
    public List<Smartphone> getUtenti() {
		return smartphones;
	}
    
	public Smartphone getSmartphone() {
		return smartphone;
	}
	
	public List<Smartphone> getSmartphones() {
		return smartphones;
	}

	public void setSmartphones(List<Smartphone> smartphones) {
		this.smartphones = smartphones;
	}

	public void setSmartphone(Smartphone smartphone) {
		this.smartphone = smartphone;
	}

	public List<String> getNomiProduttori() {
		return nomiProduttori;
	}

	public void setNomiProduttori(List<String> nomiProduttori) {
		this.nomiProduttori = nomiProduttori;
	}

	public List<Produttore> getProduttori() {
		return produttori;
	}

	public void setProduttori(List<Produttore> produttori) {
		this.produttori = produttori;
	}

	public SmartphoneService getSmartphoneService() {
		return smartphoneService;
	}

	public void setSmartphoneService(SmartphoneService smartphoneService) {
		this.smartphoneService = smartphoneService;
	}

	public ProduttoreService getProduttoreService() {
		return produttoreService;
	}

	public void setProduttoreService(ProduttoreService produttoreService) {
		this.produttoreService = produttoreService;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}
	
}

