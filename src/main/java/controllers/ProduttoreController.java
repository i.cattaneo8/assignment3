package controllers;


import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

import entities.Produttore;
import services.ProduttoreService;

import java.util.ArrayList;
import java.util.List;

public class ProduttoreController extends ActionSupport  implements Preparable{
	
	private static final long serialVersionUID = 1L;

    private ProduttoreService produttoreService = new ProduttoreService();
    private Produttore produttore;
    private List<Produttore> produttori;
    private List<String> nomiCasaMadre = new ArrayList<String>();
    private String product;

    @Override
    public void prepare() throws Exception { 
    	produttori = produttoreService.readAll();
    	nomiCasaMadre.add("Nessuno");
    	for (int i = 0; i < produttori.size(); i++) {
    		nomiCasaMadre.add(produttori.get(i).getNome());	
    	}
    	
        if (produttore != null && produttore.getId() != 0) {
        	produttore = produttoreService.read(produttore.getId());
        	nomiCasaMadre.remove(produttore.getNome());
        	if (produttore.getCasaMadre() == null) {
        		product = "Nessuno";
        	} else {
        		product = produttore.getCasaMadre().getNome();
        	}
        }
    }
    
    public void validate() {
    	if (!produttoreService.readNome(produttore.getNome()) && !(produttore.getId() != 0)) {
    		addFieldError("produttore.nome", "Produttore gia' presente nel db.");
    	}
    	
    	if (produttore.getNome() == "") {
    		addFieldError("produttore.nome", "Il nome del produttore non puo' essere vuoto.");
    	}
    }
        
	public String list() {
        produttori = produttoreService.readAll();
        return SUCCESS;
    }
	
	public String save() {
    	for (int i = 0; i < produttori.size(); i++) {
    		if (produttori.get(i).getNome().equals(product)) {
    			produttore.setCasaMadre(produttori.get(i));
    		}
    		
    		if (produttori.get(i).getNome().equals("Nessuno")) {
    			produttore.setCasaMadre(null);
    		}
    	}
    	
		if(produttoreService.read(produttore.getId()) == null) {
			produttoreService.create(produttore);
		} else {	
			produttoreService.update(produttore);
		}
		
        return SUCCESS;
    }
	
	public String delete() {
		produttoreService.delete(produttore);
        return SUCCESS;
	}

    public List<Produttore> getUtenti() {
		return produttori;
	}
    
	public Produttore getProduttore() {
		return produttore;
	}
	
	public void setProduttore(Produttore produttore) {
		this.produttore = produttore;
	}

	public List<String> getNomiCasaMadre() {
		return nomiCasaMadre;
	}

	public void setNomiCasaMadre(List<String> nomiCasaMadre) {
		this.nomiCasaMadre = nomiCasaMadre;
	}

	public List<Produttore> getProduttori() {
		return produttori;
	}

	public void setProduttori(List<Produttore> produttori) {
		this.produttori = produttori;
	}

	public ProduttoreService getProduttoreService() {
		return produttoreService;
	}

	public void setProduttoreService(ProduttoreService produttoreService) {
		this.produttoreService = produttoreService;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}
	
}

