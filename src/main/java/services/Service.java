package services;

import java.util.List;

public interface Service<T> {

    void initEntityManager();
    
    void closeEntityManager();
    
    T create(T t);
    
    T read(int id);
    
    T update(T t);
    
    void delete(T t);
    
    List<T> readAll();
    
    void deleteAll();
    
    List<T> search(String field, String value);    

}
