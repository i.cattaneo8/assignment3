package services;

import entities.Produttore;
import daos.ProduttoreDao;

import java.util.List;

public class ProduttoreService implements Service<Produttore> {

    private ProduttoreDao produttoreDao;

    public ProduttoreService() {
        this.produttoreDao = new ProduttoreDao();
        this.produttoreDao.initEntityManager();
    }

    @Override
    public void initEntityManager () {
        produttoreDao.initEntityManager();
    }
    
    @Override
    public void closeEntityManager() {
        produttoreDao.closeEntityManager();
    }
    
    @Override
    public Produttore create(Produttore produttore) {
        produttoreDao.beginTransaction();
        produttoreDao.create(produttore);
        produttoreDao.commitTransaction();
        return produttore;
    }
    
    @Override
    public Produttore read(int id) {
        produttoreDao.beginTransaction();
        Produttore produttore = produttoreDao.read(id);
        produttoreDao.commitTransaction();

        return produttore;
    }
    
    public boolean readNome(String nome) {
    	produttoreDao.beginTransaction();
        boolean produttore = produttoreDao.readNome(nome);
        produttoreDao.commitTransaction();

        return produttore;
    }
    
    @Override
    public Produttore update(Produttore produttore) {
        produttoreDao.beginTransaction();
        produttoreDao.update(produttore);
        produttoreDao.commitTransaction();
        return produttore;
    }
    
    @Override
    public void delete(Produttore produttore) {
		produttoreDao.beginTransaction();
        produttoreDao.delete(produttore);
        produttoreDao.commitTransaction();
    }

    @Override
    public List<Produttore> readAll() {
        produttoreDao.beginTransaction();
        List<Produttore> produttori = produttoreDao.readAll();
        produttoreDao.commitTransaction();

        return produttori;
    }

    @Override
    public void deleteAll () {
        produttoreDao.beginTransaction();
        produttoreDao.deleteAll();
        produttoreDao.commitTransaction();
    }
    
    public List<Produttore> search(String field, String value) {
        produttoreDao.beginTransaction();
        List<Produttore> result = produttoreDao.search(field, value);
        produttoreDao.commitTransaction();
        return result;
    }

}
