package services;

import entities.Smartphone;
import daos.SmartphoneDao;

import java.util.List;

public class SmartphoneService implements Service<Smartphone> {

    private SmartphoneDao smartphoneDao;

    public SmartphoneService() {
        this.smartphoneDao = new SmartphoneDao();
        this.smartphoneDao.initEntityManager();
    }

    @Override
    public void initEntityManager () {
        smartphoneDao.initEntityManager();
    }
    
    @Override
    public void closeEntityManager() {
        smartphoneDao.closeEntityManager();
    }
    
    @Override
    public Smartphone create(Smartphone smartphone) {
        smartphoneDao.beginTransaction();
        smartphoneDao.create(smartphone);
        smartphoneDao.commitTransaction();
        return smartphone;
    }
    
    @Override
    public Smartphone read(int id) {
        smartphoneDao.beginTransaction();
        Smartphone smartphone = smartphoneDao.read(id);
        smartphoneDao.commitTransaction();

        return smartphone;
    }
    
    @Override
    public Smartphone update(Smartphone smartphone) {
        smartphoneDao.beginTransaction();
        smartphoneDao.update(smartphone);
        smartphoneDao.commitTransaction();
        return smartphone;
    }
    
    @Override
    public void delete(Smartphone smartphone) {
        smartphoneDao.beginTransaction();
        smartphoneDao.delete(smartphone);
        smartphoneDao.commitTransaction();
    }

    @Override
    public List<Smartphone> readAll() {
        smartphoneDao.beginTransaction();
        List<Smartphone> smartphones = smartphoneDao.readAll();
        smartphoneDao.commitTransaction();

        return smartphones;
    }

    @Override
    public void deleteAll () {
        smartphoneDao.beginTransaction();
        smartphoneDao.deleteAll();
        smartphoneDao.commitTransaction();
    }

    public List<Smartphone> search(String field, String value) {
    	smartphoneDao.beginTransaction();
        List<Smartphone> result = smartphoneDao.search(field, value);
        smartphoneDao.commitTransaction();
        return result;
    }
}
