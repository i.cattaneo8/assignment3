package services;

import entities.Transazione;
import daos.TransazioneDao;

import java.util.List;

public class TransazioneService implements Service<Transazione> {

    private TransazioneDao transazioneDao;

    public TransazioneService() {
        this.transazioneDao = new TransazioneDao();
        this.transazioneDao.initEntityManager();
    }

    @Override
    public void initEntityManager () {
        transazioneDao.initEntityManager();
    }
    
    @Override
    public void closeEntityManager() {
        transazioneDao.closeEntityManager();
    }
    
    @Override
    public Transazione create(Transazione transazione) {
        transazioneDao.beginTransaction();
        transazioneDao.create(transazione);
        transazioneDao.commitTransaction();
        return transazione;
    }
    
    @Override
    public Transazione read(int id) {
        transazioneDao.beginTransaction();
        Transazione transazione = transazioneDao.read(id);
        transazioneDao.commitTransaction();

        return transazione;
    }
    
    @Override
    public Transazione update(Transazione transazione) {
        transazioneDao.beginTransaction();
        transazioneDao.update(transazione);
        transazioneDao.commitTransaction();
        return transazione;
    }
    
    @Override
    public void delete(Transazione transazione) {
        transazioneDao.beginTransaction();
        transazioneDao.delete(transazione);
        transazioneDao.commitTransaction();
    }

    @Override
    public List<Transazione> readAll() {
        transazioneDao.beginTransaction();
        List<Transazione> transazioni = transazioneDao.readAll();
        transazioneDao.commitTransaction();

        return transazioni;
    }

    @Override
    public void deleteAll () {
        transazioneDao.beginTransaction();
        transazioneDao.deleteAll();
        transazioneDao.commitTransaction();
    }

    public List<Transazione> search(String field, String value) {
    	return null;
    }
}
