package services;

import entities.Utente;
import daos.UtenteDao;

import java.util.List;

public class UtenteService implements Service<Utente> {

    private UtenteDao utenteDao;

    public UtenteService() {
        this.utenteDao = new UtenteDao();
        this.utenteDao.initEntityManager();
    }

    @Override
    public void initEntityManager () {
        utenteDao.initEntityManager();
    }
    
    @Override
    public void closeEntityManager() {
        utenteDao.closeEntityManager();
    }
    
    @Override
    public Utente create(Utente utente) {
        utenteDao.beginTransaction();
        utenteDao.create(utente);
        utenteDao.commitTransaction();
        return utente;
    }
    
    @Override
    public Utente read(int id) {
        utenteDao.beginTransaction();
        Utente utente = utenteDao.read(id);
        utenteDao.commitTransaction();

        return utente;
    }
    public boolean readMail(String email) {
        utenteDao.beginTransaction();
        boolean utente = utenteDao.readMail(email);
        utenteDao.commitTransaction();

        return utente;
    }
    
    @Override
    public Utente update(Utente utente) {
        utenteDao.beginTransaction();
        utenteDao.update(utente);
        utenteDao.commitTransaction();
        return utente;
    }
    
    @Override
    public void delete(Utente utente) {
        utenteDao.beginTransaction();
      /*  TransazioneService transazioneService = new TransazioneService();
        for(Transazione transazione : utente.getTransazione()) {
        	System.out.println("LISTA TRANSAZIONI: " + transazione);
        	transazioneService.delete(transazione);
        }*/
        utenteDao.delete(utente);
        utenteDao.commitTransaction();
    }

    @Override
    public List<Utente> readAll() {
        utenteDao.beginTransaction();
        List<Utente> utenti = utenteDao.readAll();
        utenteDao.commitTransaction();

        return utenti;
    }

    @Override
    public void deleteAll () {
        utenteDao.beginTransaction();
        utenteDao.deleteAll();
        utenteDao.commitTransaction();
    }
    
    public List<Utente> search(String field, String value) {
        utenteDao.beginTransaction();
        List<Utente> result = utenteDao.search(field, value);
        utenteDao.commitTransaction();
        return result;
    }

}
