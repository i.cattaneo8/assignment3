package daos;

import entities.Produttore;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class ProduttoreDao implements Dao<Produttore> {

    private static EntityManagerFactory emFactory;
    private static EntityManager em;

    public ProduttoreDao() {}

    @Override
    public void initEntityManager () {
        emFactory = Persistence.createEntityManagerFactory("Assignment3");
        em = emFactory.createEntityManager();
    }
    
    @Override
    public void closeEntityManager() {
    	em.close();
        emFactory.close();
    }

    @Override
    public void beginTransaction () {
    	em.getTransaction().begin();
    }

    @Override
    public void commitTransaction () {
    	em.flush();
    	em.getTransaction().commit();
    }

    @Override
    public void create(Produttore produttore) {
    	Integer lastId = em.createQuery("SELECT MAX(p.id) FROM Produttore p", Integer.class).getSingleResult();
    	if (lastId == null) {
    		lastId = 0;
    	}
    	
        produttore.setId(lastId + 1);
        produttore.setNome(produttore.getNome().toUpperCase());
    	em.persist(produttore);
    }
    
    @Override
    public Produttore read(int id) {
        return em.find(Produttore.class, id);
    }
    
    public boolean readNome(String input) {
    	List<Produttore> p = em.createNativeQuery("SELECT * FROM produttore WHERE nome = '" + input.toUpperCase()+"'", Produttore.class).getResultList();
        return p.size() == 0;
    }

    @Override
    public void update(Produttore produttore) {
    	produttore.setNome(produttore.getNome().toUpperCase());
    	em.merge(produttore);
    }

    @Override
    public void delete(Produttore produttore) {
        List<Produttore> produttori = readAll();
			for (int i = 0; i < produttori.size(); i++) {
				if (produttori.get(i).getCasaMadre() == produttore) {
					delete(produttori.get(i));
				}
			}
    	em.remove(produttore);
    }

    @Override
    public List<Produttore> readAll() {
    	return em.createNativeQuery("SELECT * FROM produttore", Produttore.class).getResultList();
    }

    @Override
    public void deleteAll() {
    	em.createNativeQuery("DELETE FROM Produttore p").executeUpdate();
    }

    @Override
    public List<Produttore> search(String field, String value) {
        return em.createQuery("SELECT p FROM Produttore p WHERE p." + field + " LIKE :value")
        	   .setParameter("value", value).getResultList();
    }

}

