package daos;

import entities.Smartphone;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class SmartphoneDao implements Dao<Smartphone> {

    private static EntityManagerFactory emFactory;
    private static EntityManager em;

    public SmartphoneDao() {}

    @Override
    public void initEntityManager () {
        emFactory = Persistence.createEntityManagerFactory("Assignment3");
        em = emFactory.createEntityManager();
    }
    
    @Override
    public void closeEntityManager() {
    	em.close();
        emFactory.close();
    }

    @Override
    public void beginTransaction () {
    	em.getTransaction().begin();
    }

    @Override
    public void commitTransaction () {
    	em.flush();
    	em.getTransaction().commit();
    }

    @Override
    public void create(Smartphone smartphone) {
    	Integer lastId = em.createQuery("SELECT MAX(s.id) FROM Smartphone s", Integer.class).getSingleResult();
    	if (lastId == null) {
    		lastId = 0;
    	}
    	
        smartphone.setId(lastId + 1);
    	em.persist(smartphone);
    }
    
    @Override
    public Smartphone read(int id) {
        return em.find(Smartphone.class, id);
    }

    @Override
    public void update(Smartphone smartphone) {
    	em.merge(smartphone);
    }

    @Override
    public void delete(Smartphone smartphone) {
    	em.remove(smartphone);
    }

    @Override
    public List<Smartphone> readAll() {
    	return em.createNativeQuery("SELECT * FROM smartphone", Smartphone.class).getResultList();
    }

    @Override
    public void deleteAll() {
    	em.createNativeQuery("DELETE FROM Smartphone s").executeUpdate();
    }

    @Override
    public List<Smartphone> search(String field, String value) {
        return em.createQuery("SELECT s FROM Smartphone s WHERE s." + field + " LIKE :value")
        	   .setParameter("value", value).getResultList();
    }

}

