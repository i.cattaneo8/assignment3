package daos;

import entities.Transazione;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class TransazioneDao implements Dao<Transazione> {

    private static EntityManagerFactory emFactory;
    private static EntityManager em;

    public TransazioneDao() {}

    @Override
    public void initEntityManager () {
        emFactory = Persistence.createEntityManagerFactory("Assignment3");
        em = emFactory.createEntityManager();
    }
    
    @Override
    public void closeEntityManager() {
    	em.close();
        emFactory.close();
    }

    @Override
    public void beginTransaction () {
    	em.getTransaction().begin();
    }

    @Override
    public void commitTransaction () {
    	em.flush();
    	em.getTransaction().commit();
    }

    @Override
    public void create(Transazione transazione) {
    	Integer lastId = em.createQuery("SELECT MAX(t.id) FROM Transazione t", Integer.class).getSingleResult();
    	if (lastId == null) {
    		lastId = 0;
    	}
    	
        transazione.setId(lastId + 1);
    	em.persist(transazione);
    }
    
    @Override
    public Transazione read(int id) {
        return em.find(Transazione.class, id);
    }

    @Override
    public void update(Transazione transazione) {
    	em.merge(transazione);
    }

    @Override
    public void delete(Transazione transazione) {
    	em.remove(transazione);
    }

    @Override
    public List<Transazione> readAll() {
    	return em.createNativeQuery("SELECT * FROM transazione", Transazione.class).getResultList();
    }

    @Override
    public void deleteAll() {
    	em.createNativeQuery("DELETE FROM Transazione t").executeUpdate();
    }
    
    @Override
    public List<Transazione> search(String field, String value) {
        return em.createQuery("SELECT t FROM Transazione t WHERE t." + field + " LIKE :value")
        	   .setParameter("value", value).getResultList();
    }

}

