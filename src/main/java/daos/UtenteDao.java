package daos;

import entities.Utente;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.commons.validator.routines.EmailValidator;


import java.util.List;

public class UtenteDao implements Dao<Utente> {

    private static EntityManagerFactory emFactory;
    private static EntityManager em;

    public UtenteDao() {}

    @Override
    public void initEntityManager () {
        emFactory = Persistence.createEntityManagerFactory("Assignment3");
        em = emFactory.createEntityManager();
    }

    @Override
    public void closeEntityManager() {
    	em.close();
        emFactory.close();
    }

    @Override
    public void beginTransaction () {
    	em.getTransaction().begin();
    }

    @Override
    public void commitTransaction () {
    	em.flush();
    	em.getTransaction().commit();
    }

    @Override
    public void create(Utente utente) {
    	Integer lastId = em.createQuery("SELECT MAX(u.id) FROM Utente u", Integer.class).getSingleResult();
    	if (lastId == null) {
    		lastId = 0;
    	}
    	
        utente.setId(lastId + 1);
        utente.setEmail(utente.getEmail().toLowerCase());
        if(EmailValidator.getInstance().isValid(utente.getEmail())) {
        	em.persist(utente);
        }
    }
    
    @Override
    public Utente read(int id) {
        return em.find(Utente.class, id);
    }
    
    public boolean readMail(String input) {
    	List<Utente> u = em.createNativeQuery("SELECT * FROM utente WHERE email = '" + input.toLowerCase()+"'", Utente.class).getResultList();
    	System.out.println(u);
        return u.size() == 0;
    }

    @Override
    public void update(Utente utente) {
        utente.setEmail(utente.getEmail().toLowerCase());
    	em.merge(utente);
    }

    @Override
    public void delete(Utente utente) {
    	em.remove(utente);
    }

    @Override
    public List<Utente> readAll() {
    	//return em.createNativeQuery("SELECT u FROM Utente u", Utente.class).getResultList();
    	return em.createNativeQuery("SELECT * FROM utente", Utente.class).getResultList();
    }

    @Override
    public void deleteAll() {
    	em.createNativeQuery("DELETE FROM Utente u").executeUpdate();
    }
    
    @Override
    public List<Utente> search(String field, String value) {
        return em.createQuery("SELECT u FROM Utente u WHERE u." + field + " LIKE :value")
        	   .setParameter("value", value).getResultList();
    }

}

