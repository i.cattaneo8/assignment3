package daos;

import java.util.List;

public interface Dao<T> {
	
    void initEntityManager();
    void closeEntityManager();
    void beginTransaction();
    void commitTransaction();
    void create(T entity);
    T read(int id);
    void update(T entity);
    void delete(T entity);
    List<T> readAll();
    void deleteAll();
    List<T> search(String field, String value);
    
}
