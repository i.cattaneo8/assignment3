package entities;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
// import org.apache.commons.validator.routines.EmailValidator;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Utente
 *
 */
@Entity
@Table(name = "UTENTE")
public class Utente implements Serializable {
   
	@Id
	private int id;
	private String nome;
	private String cognome;
	private String email;
	private String metodoPagamento;
	@OneToMany(mappedBy="utente", orphanRemoval=true, cascade=CascadeType.ALL)
	private List<Transazione> transazione =  new ArrayList<>();
	
	private static final long serialVersionUID = 1L;
	
	public Utente() {
		super();
	}   
	
	public Utente(String nome, String cognome, String email, String metodoPagamento, List<Transazione> transazione) {
		this.nome=nome;
		this.cognome=cognome;
		this.email = email;
		this.metodoPagamento=metodoPagamento;
		this.transazione=transazione;
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}   
	
	public String getCognome() {
		return this.cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}   
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getMetodoPagamento() {
		return this.metodoPagamento;
	}

	public void setMetodoPagamento(String metodoPagamento) {
		this.metodoPagamento = metodoPagamento;
	}
	
	public List<Transazione> getTransazione() {
		return transazione;
	}

	public void setTransazione(List<Transazione> transazione) {
		this.transazione = transazione;
	}
	  
	@Override
	public String toString() {
	    return "ID: " + getId() + " | "
	            + "Nome: " + getNome() + " | "
	            + " Cognome:  " + getCognome() + " | "
	            + " Email:  " + getEmail() + " | "
	            + " Metodo di pagamento: " + getMetodoPagamento() + " | "
	            + " Transazione: " + getTransazione();
	}

}
