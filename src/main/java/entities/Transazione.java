package entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Transazione
 *
 */

@Entity
@Table (name = "TRANSAZIONE")
public class Transazione implements Serializable {

	@Id
	private int id; 
	@ManyToOne(cascade=CascadeType.MERGE)
	private Utente utente;   
	private Smartphone smartphone;
	private int quantita;
	
	private static final long serialVersionUID = 1L;

	public Transazione() {
		super();
	}
	
	public Transazione(Utente utente, Smartphone smartphone, int quantita) {
		this.utente=utente;
		this.smartphone=smartphone;
		this.quantita=quantita;
		
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}   
	
	public Utente getUtente() {
		return this.utente;
	}

	public void setUtente(Utente utente) {
		this.utente = utente;
	}   
	
	public Smartphone getSmartphone() {
		return this.smartphone;
	}

	public void setSmartphone(Smartphone smartphone) {
		this.smartphone = smartphone;
	}   
	public int getQuantita() {
		return this.quantita;
	}

	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}
	
	@Override
	public String toString() {
		return "Transazione [id=" + id + ", utente=" + utente + ", smartphone=" + smartphone + ", quantita=" + quantita
				+ "]";
	}
   
}
