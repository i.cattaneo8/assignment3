package entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Produttore
 *
 */
@Entity
@Table (name = "PRODUTTORE")
public class Produttore implements Serializable {

	@Id
	private int id;
	private String nome;
	private int numeroModelli;
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private Produttore casaMadre;
	@OneToMany(mappedBy="produttore", orphanRemoval=true, cascade=CascadeType.ALL)
	private List<Smartphone> smartphone;
	
	private static final long serialVersionUID = 1L;

	public Produttore() {
		super();
	}   
	
	public Produttore(String nome, int numeroModelli, Produttore casaMadre, List<Smartphone> smartphone) {
		this.nome=nome;
		this.numeroModelli=numeroModelli;
		this.casaMadre=casaMadre;
		this.smartphone=smartphone;
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}   
	
	public int getNumeroModelli() {
		return this.numeroModelli;
	}

	public void setNumeroModelli(int numeroModelli) {
		this.numeroModelli = numeroModelli;
	}   

	public List<Smartphone> getSmartphone() {
		return smartphone;
	}

	public void setSmartphone(List<Smartphone> smartphone) {
		this.smartphone = smartphone;
	}

	public Produttore getCasaMadre() {
		return casaMadre;
	}

	public void setCasaMadre(Produttore casaMadre) {
		this.casaMadre = casaMadre;
	}
	
	@Override
	public String toString() {
		return "Produttore [id=" + id + ", nome=" + nome + ", numeroModelli=" + numeroModelli + ", casaMadre="
				+ casaMadre + ", smartphone=" + smartphone + "]";
	}

}
