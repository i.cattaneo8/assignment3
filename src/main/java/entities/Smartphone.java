package entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Smartphone
 *
 */
@Entity
@Table (name = "SMARTPHONE")
public class Smartphone implements Serializable {
 
	@Id
	private int id;
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private Produttore produttore;
	private String modello;
	private Integer anno;
	private float prezzo;
	@OneToMany(mappedBy="smartphone", orphanRemoval=true, cascade=CascadeType.ALL)
	private List<Transazione> transazione;
	
	private static final long serialVersionUID = 1L;
	
	public Smartphone() {
		super();
	}  
	
	public Smartphone(Produttore produttore, String modello, Integer anno, float prezzo, List<Transazione> transazione) {
		this.produttore=produttore;
		this.modello=modello;
		this.anno=anno;
		this.prezzo=prezzo;
		this.transazione=transazione;
	}   
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	
	public Produttore getProduttore() {
		return this.produttore;
	}

	public void setProduttore(Produttore produttore) {
		this.produttore = produttore;
	}   
	public String getModello() {
		return this.modello;
	}

	public void setModello(String modello) {
		this.modello = modello;
	}   
	
	public Integer getAnno() {
		return this.anno;
	}

	public void setAnno(Integer anno) {
		this.anno = anno;
	}   
	
	public float getPrezzo() {
		return this.prezzo;
	}

	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}

	public List<Transazione> getTransazione() {
		return transazione;
	}

	public void setTransazione(List<Transazione> transazione) {
		this.transazione = transazione;
	}
	
	@Override
	public String toString() {
		return "Smartphone [id=" + id + ", produttore=" + produttore + ", modello=" + modello + ", anno=" + anno
				+ ", prezzo=" + prezzo + ", transazione=" + transazione + "]";
	}
	
}
