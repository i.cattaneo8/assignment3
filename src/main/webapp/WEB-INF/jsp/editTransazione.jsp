<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<s:head />
        <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
        <style>td { white-space:nowrap; }</style>
        <title>Nuova transazione</title>
</head>
<body>
<s:form action="saveTransazione" method="post">
  	<s:select  name="utentenome" list="nomiUtenti" label="Utente"/><br/>  	
  	<s:select  name="smartphonemodello" list="modelliSmartphone" label="Smarthpone"/><br/>  	
  	<s:select name="transazione.quantita" list="quantita" label="Quantita'"/><br/>
  	  	
  	<s:hidden name="transazione.id" value="%{transazione.id}"/>
  	

            <s:submit value="Save"/>
            
            
        </s:form>
        <p><a href='<s:url action="transazioni" />' >Annulla</a></p>
</body>
</html>