<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
     <s:head />
        <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>   
    <title>Produttore</title>
    </head>
    <body>
<h1>Lista produttori</h1>

    <s:url var="url" action="inputProduttore" />
    <a href="<s:property value="#url"/>">Aggiungi un nuovo produttore</a> <br/><br/>
                   
  
 <table class="borderAll">
           <tr>
                <th>Nome</th>
                <th>Numero modelli</th>
                <th>Casa madre</th>
                <th>&nbsp;</th>
            </tr>

            <s:iterator value="produttori" >
                <tr>
                    <td class="nowrap"><s:property value="nome"/></td>
                    <td class="nowrap"><s:property value="numeroModelli"/></td>
                    <td class="nowrap"><s:property value="casaMadre.getNome()"/></td>
                    <td class="nowrap">
                    
                        <s:url action="inputProduttore" var="url">
                            <s:param name="produttore.id" value="id"/>
                        </s:url>
                        <a href="<s:property value="#url"/>">Edit</a>
                        
                        &nbsp;&nbsp;&nbsp;
                        <s:url action="deleteProduttore" var="url">
                            <s:param name="produttore.id" value="id"/>
                        </s:url>
                        <a href="<s:property value="#url"/>">Delete</a>
                    </td>           
           
            </tr>
</s:iterator>
</table>

    <h4><a href="index.jsp">Torna alla Home</a></h4>

    </body>
</html>
