<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
     <s:head />
        <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>   
    <title>Utenti</title>
    </head>
    <body>
<h1>Lista utenti</h1>

    <s:url var="url" action="inputUtente" />
    <a href="<s:property value="#url"/>">Aggiungi un nuovo utente</a><br/><br/>
  
 <table class="borderAll">
           <tr>
                <th>Nome</th>
                <th>Cognome</th>
                <th>Email</th>
                <th>Metodo di pagamento</th>
                <th>&nbsp;</th>
            </tr>

            <s:iterator value="utenti" >
                <tr>
                    <td class="nowrap"><s:property value="nome"/></td>
                    <td class="nowrap"><s:property value="cognome"/></td>
                    <td class="nowrap"><s:property value="email"/></td>
                    <td class="nowrap"><s:property value="metodoPagamento"/></td>                    
                    <td class="nowrap">
                    
                        <s:url action="inputUtente" var="url">
                            <s:param name="utente.id" value="id"/>
                        </s:url>
                        <a href="<s:property value="#url"/>">Edit</a>
                        
                        &nbsp;&nbsp;&nbsp;
                        <s:url action="deleteUtente" var="url">
                            <s:param name="utente.id" value="id"/>
                        </s:url>
                        <a href="<s:property value="#url"/>">Delete</a>
                    </td>           
           
            </tr>
</s:iterator>
</table>

    <h4><a href="index.jsp">Torna alla Home</a></h4>

    </body>
</html>
