<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<s:head />
        <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
        <style>td { white-space:nowrap; }</style>
        <title>Nuovo utente</title>
</head>
<body>
<s:form action="saveUtente" method="post">
	<p><s:textfield name="utente.nome" label="Nome" /> </p><br />
  	<p><s:textfield name="utente.cognome" label="Cognome" /></p> <br/>
  	<p><s:textfield name="utente.email" label="Email" /></p> <br/>
  	<s:select name="utente.metodoPagamento" list="metods" label="Metodo di pagamento"/><br/>
  	<s:hidden name="utente.id" value="%{utente.id}"/>
  	

            <s:submit value="Save"/>
            
            
        </s:form>
        <p><a href='<s:url action="utenti" />' >Annulla</a></p>
</body>
</html>