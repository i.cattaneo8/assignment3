<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
     <s:head />
        <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>   
    <title>Transazione</title>
    </head>
    <body>
<h1>Lista Transazione</h1>

    <s:url var="url" action="inputTransazione" />
    <a href="<s:property value="#url"/>">Aggiungi una nuova transazione</a><br/><br/>
  
 <table class="borderAll">
           <tr>
                <th>Utente</th>
                <th>Smartphone</th>
                <th>Quantità</th>
                <th>&nbsp;</th>
            </tr>

            <s:iterator value="transazioni" >
                <tr>
                    <td class="nowrap"><s:property value="utente.getNome()"/></td>
                    <td class="nowrap"><s:property value="smartphone.getModello()"/></td>
                    <td class="nowrap"><s:property value="quantita"/></td>
                    <td class="nowrap">
                    
                        <s:url action="inputTransazione" var="url">
                            <s:param name="transazione.id" value="id"/>
                        </s:url>
                        <a href="<s:property value="#url"/>">Edit</a>
                        
                        &nbsp;&nbsp;&nbsp;
                        <s:url action="deleteTransazione" var="url">
                            <s:param name="transazione.id" value="id"/>
                        </s:url>
                        <a href="<s:property value="#url"/>">Delete</a>
                    </td>           
           
            </tr>
</s:iterator>
</table>

    <h4><a href="index.jsp">Torna alla Home</a></h4>

    </body>
</html>
