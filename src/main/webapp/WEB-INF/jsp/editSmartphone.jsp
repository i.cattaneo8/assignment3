<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<s:head />
        <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
        <style>td { white-space:nowrap; }</style>
        <title>Nuovo smartphone</title>
</head>
<body>
<s:form action="saveSmartphone" method="post">
  	<s:select  name="product" list="nomiProduttori" label="Produttore"/><br/>  	
  	<p><s:textfield name="smartphone.modello" label="Modello" /></p> <br/>
  	<p><s:textfield name="smartphone.anno" label="Anno" /></p> <br/>
  	<p><s:textfield name="smartphone.prezzo" label="Prezzo" /></p> <br/>
  	
  	<s:hidden name="smartphone.id" value="%{smartphone.id}"/>
  	

            <s:submit value="Save"/>
            
            
        </s:form>
        <p><a href='<s:url action="smartphones" />' >Annulla</a></p>
</body>
</html>