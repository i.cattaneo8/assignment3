<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
     <s:head />
        <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>   
    <title>Smartphone</title>
    </head>
    <body>
<h1>Lista smartphone</h1>

    <s:url var="url" action="inputSmartphone" />
    <a href="<s:property value="#url"/>">Aggiungi un nuovo smartphone</a> <br/><br/>
  
 <table class="borderAll">
           <tr>
                <th>Produttore</th>
                <th>Modello</th>
                <th>Anno</th>
                <th>Prezzo</th>
                <th>&nbsp;</th>
            </tr>

            <s:iterator value="smartphones" >
                <tr>
                    <td class="nowrap"><s:property value="produttore.getNome()"/></td>
                    <td class="nowrap"><s:property value="modello"/></td>
                    <td class="nowrap"><s:property value="anno"/></td>
                    <td class="nowrap"><s:property value="prezzo"/></td>                    
                    <td class="nowrap">
                    
                        <s:url action="inputSmartphone" var="url">
                            <s:param name="smartphone.id" value="id"/>
                        </s:url>
                        <a href="<s:property value="#url"/>">Edit</a>
                        
                        &nbsp;&nbsp;&nbsp;
                        <s:url action="deleteSmartphone" var="url">
                            <s:param name="smartphone.id" value="id"/>
                        </s:url>
                        <a href="<s:property value="#url"/>">Delete</a>
                    </td>           
           
            </tr>
</s:iterator>
</table>

    <h4><a href="index.jsp">Torna alla Home</a></h4>

    </body>
</html>
